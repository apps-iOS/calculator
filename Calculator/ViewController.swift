//
//  ViewController.swift
//  Calculator
//
//  Created by Daniel on 25/02/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var buttonAC: UIButton!
    @IBOutlet weak var buttonDEL: UIButton!
    @IBOutlet weak var buttonPercent: UIButton!
    @IBOutlet weak var buttonDivisor: UIButton!
    @IBOutlet weak var buttonMultiplication: UIButton!
    @IBOutlet weak var buttonSubstraction: UIButton!
    @IBOutlet weak var buttonSum: UIButton!
    @IBOutlet weak var button0: UIButton!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var button5: UIButton!
    @IBOutlet weak var button6: UIButton!
    @IBOutlet weak var button7: UIButton!
    @IBOutlet weak var button8: UIButton!
    @IBOutlet weak var button9: UIButton!
    @IBOutlet weak var buttonComa: UIButton!
    @IBOutlet weak var buttonResult: UIButton!

    @IBOutlet weak var labelDisplay: UILabel!
    @IBOutlet weak var labelResult: UILabel!
    var operation: Bool = false
    var result: Bool = false
    var sum: Bool = false
    var substraction: Bool = false
    var multiplication: Bool = false
    var division: Bool = false
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.buttonAC.round()
        self.buttonDEL.round()
        self.buttonPercent.round()
        self.buttonDivisor.round()
        self.buttonMultiplication.round()
        self.buttonSubstraction.round()
        self.buttonSum.round()
        self.button0.round()
        self.button1.round()
        self.button2.round()
        self.button3.round()
        self.button4.round()
        self.button5.round()
        self.button6.round()
        self.button7.round()
        self.button8.round()
        self.button9.round()
        self.buttonComa.round()
        self.buttonResult.round()
    }

    @IBAction func numberAction(_ sender: UIButton) {
        if self.labelDisplay.text == "0" || self.result {
            self.labelDisplay.text = ""
            self.labelResult.text = ""
        }
        self.labelDisplay.text = "\(self.labelDisplay.text!)" + String(sender.tag)
        if self.sum || self.substraction || self.multiplication || self.division {
            self.calculate()
        }
        sender.shine()
    }
    
    @IBAction func comaAction(_ sender: UIButton) {
        if self.labelDisplay.text == "" {
            self.labelDisplay.text = "0"
        }
        self.labelDisplay.text = "\(self.labelDisplay.text!),"
    }
    
    @IBAction func acAction(_ sender: UIButton) {
        self.labelResult.text = ""
        self.labelDisplay.text = "0"
        self.operation = false
        self.result = false
        self.sum = false
        self.substraction = false
        self.multiplication = false
        self.division = false
        sender.shine()
    }
    
    @IBAction func deleteAction(_ sender: UIButton) {
        if self.labelDisplay.text != "" {
            if self.labelDisplay.text?.last == " " {
                self.labelDisplay.text = String(self.labelDisplay.text!.dropLast(3))
            }else{
                self.labelDisplay.text = String(self.labelDisplay.text!.dropLast())
            }
            //self.labelResult.text = ""
        }
        self.result = false
        sender.shine()
    }
    
    @IBAction func sumAction(_ sender: UIButton) {
        if self.labelDisplay.text != "" {
            if self.result && self.labelResult.text != "" {
                self.labelDisplay.text = self.labelResult.text
            }
            if self.labelDisplay.text?.last == " " && self.operation {
                self.labelDisplay.text = String(self.labelDisplay.text!.dropLast(3))
            }
            self.labelDisplay.text = "\(self.labelDisplay.text!) + "
            self.operation = true
            self.result = false
            self.sum = true
        }
        sender.shine()
    }
    
    @IBAction func subtractionAction(_ sender: UIButton) {
        if self.labelDisplay.text != "" {
            if self.result && self.labelResult.text != "" {
                self.labelDisplay.text = self.labelResult.text
            }
            if self.labelDisplay.text?.last == " " && self.operation {
                self.labelDisplay.text = String(self.labelDisplay.text!.dropLast(3))
            }
            self.labelDisplay.text = "\(self.labelDisplay.text!) – "
            self.operation = true
            self.result = false
            self.substraction = true
        }
        sender.shine()
    }
    
    @IBAction func multiplicationAction(_ sender: UIButton) {
        if self.labelDisplay.text != "" {
            if self.result && self.labelResult.text != "" {
                self.labelDisplay.text = self.labelResult.text
            }
            if self.labelDisplay.text?.last == " " && self.operation {
                self.labelDisplay.text = String(self.labelDisplay.text!.dropLast(3))
            }
            self.labelDisplay.text = "\(self.labelDisplay.text!) × "
            self.operation = true
            self.result = false
            self.multiplication = true
        }
        sender.shine()
    }
    
    @IBAction func divisionAction(_ sender: UIButton) {
        if self.labelDisplay.text != "" {
            if self.result && self.labelResult.text != "" {
                self.labelDisplay.text = self.labelResult.text
            }
            if self.labelDisplay.text?.last == " " && self.operation {
                self.labelDisplay.text = String(self.labelDisplay.text!.dropLast(3))
            }
            self.labelDisplay.text = "\(self.labelDisplay.text!) ÷ "
            self.operation = true
            self.result = false
            self.division = true
        }
        sender.shine()
    }
    
    @IBAction func resultAction(_ sender: UIButton) {
        self.calculate()
        self.labelDisplay.text = self.labelResult.text
        self.labelResult.text = ""
        sender.shine()
    }
    
    private func calculate(){
        if self.labelDisplay.text?.last != " " {
            let aString = self.labelDisplay.text!
            var newString = aString.replacingOccurrences(of: "×", with: "*")
            newString = newString.replacingOccurrences(of: "÷", with: "/")
            newString = newString.replacingOccurrences(of: "–", with: "-")
            newString = newString.replacingOccurrences(of: ",", with: ".")
            
            let expn = NSExpression(format:newString)
            print(expn)
            
            if let result = expn.expressionValue(with: nil, context: nil) as? NSNumber {
                let x = result.doubleValue
                let disp = String(x.clean).replacingOccurrences(of: ".", with: ",")
                self.labelResult.text = disp
                print(x)
                self.result = true
            } else {
                print("failed")
            }
        }
    }
}

// MARK: -
extension Double {
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

extension UIButton {
    
    // Borde redondo
    func round() {
        layer.cornerRadius = bounds.height / 2
        clipsToBounds = true
    }
    
    // Brilla
    func shine() {
        UIView.animate(withDuration: 0.1, animations: {
            self.alpha = 0.5
        }) { (completion) in
            UIView.animate(withDuration: 0.1, animations: {
                self.alpha = 1
            })
        }
    }
}
